import { fetchPokemonList } from './pokemonprovider.js';

function filtrerPokemonPoids(pokemons) {
    return pokemons.filter(pokemon => pokemon.weight > 3);
}

function filtrerPokemonTaille(pokemons) {
    return pokemons.filter(pokemon => pokemon.height < 1);
}

function afficherPokemonDansTableau(pokemons) {
    const tbody = document.querySelector("#pokemon-table tbody");
    tbody.innerHTML = "";

    pokemons.forEach(pokemon => {
        const row = document.createElement("tr");
        const nameCell = document.createElement("td");
        const weightCell = document.createElement("td");
        const heightCell = document.createElement("td");
        
        nameCell.textContent = pokemon.name;
        weightCell.textContent = pokemon.weight;
        heightCell.textContent = pokemon.height;
        
        row.appendChild(nameCell);
        row.appendChild(weightCell);
        row.appendChild(heightCell);
        
        tbody.appendChild(row);
    });
}

document.getElementById("btn-filtrer-poids").addEventListener("click", function() {
    fetchPokemonList(pokemons => {
        const pokemonsFiltres = filtrerPokemonPoids(pokemons);
        afficherPokemonDansTableau(pokemonsFiltres);
    });
});

document.getElementById("btn-filtrer-taille").addEventListener("click", function() {
    fetchPokemonList(pokemons => {
        const pokemonsFiltres = filtrerPokemonTaille(pokemons);
        afficherPokemonDansTableau(pokemonsFiltres);
    });
});

document.getElementById("btn-filtrer-tous").addEventListener("click", function() {
    fetchPokemonList(afficherPokemonDansTableau);
});

fetchPokemonList(afficherPokemonDansTableau);

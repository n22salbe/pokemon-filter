function fetchPokemonList(callback, limit = 20) {
    url = baseApiUrl + "?limit=" + limit;
    const request = async() => {
         const response = await fetch(url);
         const json = await response.json();
         let promisesArray = json["results"].map(result => {
            return fetch(result.url).then(response => response.json());
        });
        return Promise.all(promisesArray);
    };
    request().then((data) => {
        console.log("Données récupérées :", data); // Vérifier les données récupérées
        let pokemons = [];
        for (let i = 0; i < data.length; ++i) {
            pokemons.push({"name": data[i].name, "weight": data[i].weight, "height": data[i].height});
        }
        callback(pokemons);
    });  
}


exports.fetchPokemonList = fetchPokemonList;